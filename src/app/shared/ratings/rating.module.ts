import { NgModule} from "@angular/core";
//import {ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, AbstractControl} from "@angular/forms";
import {CommonModule} from "@angular/common";

import { RatingComponent } from './rating.component'

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        RatingComponent,
    ],
    exports: [
        RatingComponent,
    ],
})
export class RatingModule {

}