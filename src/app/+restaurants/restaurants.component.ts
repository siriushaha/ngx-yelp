import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';

// import { getPaginatedRestaurants, getRestaurants, getLoading, getCurrentPage, getPageTotal, getPages, getTerm, getLocation, getSortBy } from './store/reducers';

import * as fromRestaurantReducers from './store/reducers';
import { IRestaurant } from './models';

@Component({
  selector: 'restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css']
})
export class RestaurantsComponent implements OnInit {

  loading$: Observable<boolean>;
  term$: Observable<string>;
  location$: Observable<string>;
  sortby$: Observable<string>
  restaurants$: Observable<IRestaurant[]>;
  paginatedRestaurants$: Observable<IRestaurant[]>;
  // page$: Observable<number>;
  // pageTotal$: Observable<number>;
  // pages$: Observable<number[]>;
  
  constructor(private store: Store<fromRestaurantReducers.RestaurantState>) {
    this.paginatedRestaurants$ = store.select(fromRestaurantReducers.getPagedRestaurants);
    this.restaurants$ = store.select(fromRestaurantReducers.getRestaurants);
    this.loading$ = store.select(fromRestaurantReducers.getRestaurantLoading);
    // this.page$ = store.select(fromRestaurantReducers.getCurrentPage);
    // this.pageTotal$ = store.select(fromRestaurantReducers.getPageTotal);
    // this.pages$ = store.select(fromRestaurantReducers.getPages);
    this.term$ = store.select(fromRestaurantReducers.getTerm);
    this.location$ = store.select(fromRestaurantReducers.getLocation);
    this.sortby$ = store.select(fromRestaurantReducers.getSortBy);
  }

  ngOnInit() {
  }

}
