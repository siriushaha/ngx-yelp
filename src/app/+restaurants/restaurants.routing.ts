import { Routes, RouterModule } from '@angular/router';
import { RestaurantsComponent } from "./restaurants.component";
import { ModuleWithProviders } from "@angular/core";

export const restaurantsRoutes: Routes = [
    {
        path: '',
        component: RestaurantsComponent,
        data: {
            pageTitle: 'Restaurants'
        }
    }
];

export const restaurantsRouting: ModuleWithProviders = RouterModule.forChild(restaurantsRoutes);

