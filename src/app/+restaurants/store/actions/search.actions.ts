
import { Action } from '@ngrx/store';

export enum SearchActionTypes {
  SET_TERM =     'SET_TERM',
  SET_LOCATION = 'SET_LOCATION',
  SET_SORT_BY =  'SET_SORT_BY'
}

export class SearchSetTermAction implements Action  {
  readonly type = SearchActionTypes.SET_TERM;

  constructor(public payload: string) {}
}

export class SearchSetLocationAction implements Action {
  readonly type = SearchActionTypes.SET_LOCATION;

  constructor(public payload: string) {}
}

export class SearchSetSortByAction implements Action {
  readonly type = SearchActionTypes.SET_SORT_BY;

  constructor(public payload: string) {}

}

export type SearchActions =
  SearchSetTermAction |
  SearchSetLocationAction |
  SearchSetSortByAction
;
