import { Action } from '@ngrx/store';
import { IRestaurant} from '../../models';

export enum RestaurantActionTypes {
  SEARCH_RESTAURANTS =         'SEARCH_RESTAURANTS',
  SEARCH_RESTAURANTS_SUCCESS = 'SEARCH_RESTAURANTS_SUCCESS',
  SEARCH_RESTAURANTS_FAILED =  'SEARCH_RESTAURANTS_FAILED',
  GOTO_RESTAURANT_PAGE =       'GOTO_RESTAURANT_PAGE',
  NEXT_RESTAURANT_PAGE =       'NEXT_RESTAURANT_PAGE',
  PREV_RESTAURANT_PAGE =       'PREV_RESTAURANT_PAGE'
}

export class SearchRestaurantsAction implements Action {
  readonly type = RestaurantActionTypes.SEARCH_RESTAURANTS;
  payload: any;

  constructor(location: string, term: string, sortby: string) {
    this.payload = {location, term, sortby};
  }
}

export class SearchRestaurantsSuccessAction implements Action {
  readonly type = RestaurantActionTypes.SEARCH_RESTAURANTS_SUCCESS;
  payload: IRestaurant[];

  constructor(searchResult: any) {
    this.payload = <IRestaurant[]> searchResult.restaurants.businesses;
  }
}

export class SearchRestaurantsFailedAction implements Action {
  readonly type = RestaurantActionTypes.SEARCH_RESTAURANTS_FAILED;
  constructor(public payload: any) {}
}

export class GotoRestaurantPageAction implements Action {
  readonly type = RestaurantActionTypes.GOTO_RESTAURANT_PAGE;

  constructor(public payload: number) {}
}

export class GotoNextRestaurantPageAction implements Action {
  readonly type = RestaurantActionTypes.NEXT_RESTAURANT_PAGE;
}


export class GotoPrevRestaurantPageAction  implements Action {
  readonly type =  RestaurantActionTypes.PREV_RESTAURANT_PAGE;
}

/*
  static SORT_SITES = 'SORT_SITES'
  sortSites(by: string): Action {
    return {
      type: SiteActions.SORT_SITES,
      payload: by
    }
  }

  static FILTER_SITES = 'FILTER_SITES'
  filterSites(filter: string): Action {
    return {
      type: SiteActions.FILTER_SITES,
      payload: filter
    }
  }
*/

export type RestaurantActions =
  SearchRestaurantsAction |
  SearchRestaurantsSuccessAction |
  SearchRestaurantsFailedAction |
  GotoRestaurantPageAction |
  GotoNextRestaurantPageAction |
  GotoPrevRestaurantPageAction
;

