import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects'
import * as fromRestaurantActions from '../actions'
import { RestaurantService } from '../../services'
import { IRestaurant } from '../../models';

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';


@Injectable()
export class RestaurantEffects {
	
  constructor (
    private action$: Actions,
    private restaurantService: RestaurantService
  ) {}

  @Effect()
  searchRestaurants$: Observable<Action> = this.action$
    .ofType(fromRestaurantActions.RestaurantActionTypes.SEARCH_RESTAURANTS)
    .map((action: fromRestaurantActions.SearchRestaurantsAction) => action.payload)
    .switchMap((payload) => {
      return this.restaurantService.searchRestaurants(payload.location, payload.term, payload.sortby)
        .map((restaurants: IRestaurant[]) => new fromRestaurantActions.SearchRestaurantsSuccessAction(restaurants))
        .catch(error => Observable.of(new fromRestaurantActions.SearchRestaurantsFailedAction(error)));
    });
}
