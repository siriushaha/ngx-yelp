import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as fromSearch from './search.reducers';
import * as fromRestaurant from './restaurant.reducers';
import * as fromRoot from '../../../core/store/reducers';

/**
 * Interface containing all states within Restaurant feature
 */
export interface RestaurantState {
  search: fromSearch.State;
  restaurants: fromRestaurant.State;
}

/**
 * Interface for state of Batch Feature
 */
export interface State extends fromRoot.State {
  'restaurant': RestaurantState;
}

/**
 * Combined reducers containing all reducers within Restaurant feature
 * @type {{jobs: ((state: State, action: JobActions) => State); batch_inputs: ((state: State, action: BatchInputActions) => State)}}
 */
export const reducers = {
  search: fromSearch.reducer,
  restaurants: fromRestaurant.reducer
};

/**
 * A selector function is a map function factory. We pass it parameters and it
 * returns a function that maps from the larger state tree into a smaller
 * piece of state. This selector simply selects the `books` state.
 *
 * Selectors are used with the `select` operator.
 *
 * ```ts
 * class MyComponent {
 * 	constructor(state$: Observable<State>) {
 * 	  this.booksState$ = state$.select(getBooksState);
 * 	}
 * }
 * ```
 */

/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
 */

/**
 * Feature selector for Restaurant feature
 * @type {MemoizedSelector<Object, BatchState>}
 */
export const getRestaurantState = createFeatureSelector<RestaurantState>('restaurant');

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them useable, we
 * need to make new selectors that wrap them.
 *
 * The createSelector function creates very efficient selectors that are memoized and
 * only recompute when arguments change. The created selectors can also be composed
 * together to select different pieces of state.
 */

/**
 ********** Restaurant state **********
 */

/**
 * Creator selector for Restaurants state
 */
export const getRestaurantsState = createSelector(
  getRestaurantState,
  (state: RestaurantState) => state.restaurants
);

/**
 * Creator selector for Restaurant Loading
 * @type {MemoizedSelector<Object, boolean>}
 */
export const getRestaurantLoading = createSelector(
  getRestaurantsState,
  fromRestaurant.getRestaurantLoading
);

/**
 * Creator selector for Restaurant Error Loading
 * @type {MemoizedSelector<Object, string>}
 */
export const getJobErrorLoading = createSelector(
  getRestaurantsState,
  fromRestaurant.getRestaurantError
);

/**
 * Creator selector for Restaurants
 * @type {MemoizedSelector<Object, IJobTypes>}
 */
export const getRestaurants = createSelector(
  getRestaurantsState,
  fromRestaurant.getRestaurants
);

/**
 * Creator selector for filtered restaurants
 * @type {MemoizedSelector<Object, IJobTypes>}
 */
export const getFilteredRestaurants = createSelector(
  getRestaurantsState,
  fromRestaurant.getFilteredRestaurants
);

/**
 * Creator selector for paged restaurants
 * @type {MemoizedSelector<Object, IJobTypes>}
 */
export const getPagedRestaurants = createSelector(
  getRestaurantsState,
  fromRestaurant.getPagedRestaurants
);

/**
 * Creator selector for page index
 * @type {MemoizedSelector<Object, number>}
 */
export const getPageIndex = createSelector(
  getRestaurantsState,
  fromRestaurant.getPageIndex
);

/**
 * Creator selector for page size
 * @type {MemoizedSelector<Object, number>}
 */
export const getPageSize = createSelector(
  getRestaurantsState,
  fromRestaurant.getPageSize
);

/**
 ********** Search state **********
 */

/**
 * Creator selector for Search state
 * @type {MemoizedSelector<Object, State>}
 */
export const getSearchState = createSelector(
  getRestaurantState,
  (state: RestaurantState) => state.search
);

/**
 * Creator selector for Term
 * @type {MemoizedSelector<Object, boolean>}
 */
export const getTerm = createSelector(
  getSearchState,
  fromSearch.getTerm
);

/**
 * Creator selector for Location
 * @type {MemoizedSelector<Object, IOptions>}
 */
export const getLocation = createSelector(
  getSearchState,
  fromSearch.getLocation
);

/**
 * Creator selector for SortBy
 * @type {MemoizedSelector<Object, string[]>}
 */
export const getSortBy = createSelector(
  getSearchState,
  fromSearch.getSortBy
);


/**
 * Some selector functions create joins across parts of state. This selector
 * composes the search result IDs to return an array of books in the store.
 */
