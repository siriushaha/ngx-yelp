import { PayloadAction } from '../../../core/store/actions/payload-action.actions';
import { SearchActionTypes } from '../actions'

export interface State {
  term: string | null;
  location: string | null;
  sortby: string;
}

const initialState: State = {
  term: 'restaurants',
  location: null,
  sortby: 'best_match'
}

export function reducer(state: State = initialState, action: PayloadAction): State {
  switch (action.type) {
    case SearchActionTypes.SET_TERM :
      return setTerm(state, action.payload);

    case SearchActionTypes.SET_LOCATION:
      return setLocation(state, action.payload);

    case SearchActionTypes.SET_SORT_BY:
      return setSortBy(state, action.payload);

    default:
      return state;
  }
}

function setTerm(state: State, term: string): State {
  const newState: State = Object.assign({}, state, { term });
  return newState;
}

function setLocation(state: State, location: string): State {
  const newState: State = Object.assign({}, state, { location });
  return newState;
}

function setSortBy(state: State, sortby: string): State {
  const newState: State = Object.assign({}, state, { sortby });
  return newState;
}

export const getTerm = (state: State) => state.term;

export const getLocation = (state: State) => state.location;

export const getSortBy = (state: State) => state.sortby;

