
import { Observable } from 'rxjs/Observable';
import { ActionReducer, Action } from '@ngrx/store';
import { IRestaurant } from '../../models';
import { PayloadAction } from '../../../core/store/actions/payload-action.actions';
import * as restaurantActions from '../actions/restaurant.actions';

import { RestaurantActionTypes } from '../actions/restaurant.actions'

import {

  IPaginatedState,
  totalPages,
  slicedList,
  filterList,
  removeList,
  initList,
  paginateList
} from '../../../core/store/shared/pagination';

const _ = require('lodash');

export interface State extends IPaginatedState {
  loading?: boolean;
  error?: string;
}

const initialState: State = {
  list: [],
  loading: false,
  error: null
}

export function reducer(state: State = initialState, action: PayloadAction): State {
  switch (action.type) {
    case RestaurantActionTypes.SEARCH_RESTAURANTS_SUCCESS :
      return searchRestaurantsSuccess(state, <IRestaurant[]> action.payload);

    case RestaurantActionTypes.SEARCH_RESTAURANTS:
      return searchRestaurants(state);

    default:
      return state;
  }
}

function searchRestaurants(state: State): State {
  const newState: State = Object.assign({}, state, { loading: false });
  return newState;
}

function searchRestaurantsSuccess(state: State, restaurants: IRestaurant[]): State {
  const newList: IRestaurant[] = [...restaurants.map(job => Object.assign({}, job, { selected: false}))];
  const newState: State = Object.assign({}, initList(state, newList), { loading: true, error: null});
  return newState;
}

/*
export function getLoadingFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.loading);
}

export function getRestaurantsFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.list);
}

export function getPaginatedRestaurantsFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.pageList);
}

export function getCurrentPageFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.page);
}

export function getPageTotalFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.total);
}

export function getPagesFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.pages);
}

export function getCountPerPageFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.per);
}

export const paginatedReducer = paginate(reducer, {
  GOTO: RestaurantActionTypes.GOTO_RESTAURANT_PAGE,
  NEXT: RestaurantActionTypes.NEXT_RESTAURANT_PAGE,
  PREV: RestaurantActionTypes.PREV_RESTAURANT_PAGE
}, {
  defaultPage: 1,
  defaultPer: 10,
  defaultTotal: 0
})

*/

/**
 * Selector for JobLoading from Restaurant state
 * @param {State} state
 */
export const getRestaurantLoading = (state: State) => state.loading;

/**
 * Selector for JobErrorLoading from Restaurant state
 * @param {State} state
 */
export const getRestaurantError = (state: State) => state.error;

/**
 * Selector for Jobs from Restaurant state
 * @param {State} state
 */
export const getRestaurants = (state: State) => state.list;

/**
 * Selector for filtered from Restaurant state
 * @param {State} state
 */
export const getFilteredRestaurants = (state: State) => state.filtered_list;

/**
 * Selector for paginated from Restaurant state
 * @param {State} state
 */
export const getPagedRestaurants = (state: State) => state.paged_list;

/**
 * Selector for page index from Restaurant state
 * @param {State} state
 */
export const getPageIndex = (state: State) => state.page - 1;

/**
 * Selector for page index from Restaurant state
 * @param {State} state
 */
export const getPageSize = (state: State) => state.per;

