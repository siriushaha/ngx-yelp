import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { RatingModule } from '../shared/ratings/rating.module';

import { SmartadminModule } from '../shared/smartadmin.module';

import { restaurantsRouting } from './restaurants.routing';

import { RestaurantsComponent} from './restaurants.component';
import { SearchComponent, RestaurantSummaryComponent, RestaurantComponent } from './components';

import { reducers } from './store/reducers';
import { RestaurantEffects } from './store/effects/restaurant.effects';
import { RestaurantService } from './services';

@NgModule({
  imports: [
    CommonModule,
    restaurantsRouting,
    RatingModule,
    SmartadminModule,

    StoreModule.forFeature( 'restaurant', reducers),
    EffectsModule.forFeature([RestaurantEffects]),
  ],
  declarations: [RestaurantsComponent, SearchComponent, RestaurantSummaryComponent, RestaurantComponent],
  exports: [RestaurantsComponent],
  providers: [
    RestaurantService
  ]
})
export class RestaurantsModule {
}
