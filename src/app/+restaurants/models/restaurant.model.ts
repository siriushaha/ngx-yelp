export interface IRestaurant {
  id: string;
  name: string;
  image_url: string;
  url: string;
  price: string;
  phone: string;
  rating: number;
  review_count: number;
  distance: number;
  categories: ICategory[];
  location: ILocation;
  coordinates?: ICoordinate;
  selected?: boolean;
}

export interface ICategory {
	title: string;
}

export interface ILocation {
	display_address: string[];
}

export interface ICoordinate {
	longitude: number;
  latitude: number;
}

export interface IMap {
	key: string;
	value: string;
}

export enum SortBy {
	Rating,
	Distance,
	Review_Count,
	Best_Match
}
