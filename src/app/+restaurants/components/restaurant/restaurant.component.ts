import { Component, Input,  OnInit, ChangeDetectionStrategy } from '@angular/core';

import { IRestaurant } from '../../models';

@Component({
  selector: 'restaurant-info',
  templateUrl: './restaurant.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class RestaurantComponent implements OnInit {

  @Input() restaurant: IRestaurant;
  @Input() index: number;

  categories: string;
  distance_in_miles: number;
  constructor() { }

  ngOnInit() {
    this.categories = this.restaurant.categories.map(c => c.title).join(', ');
    this.distance_in_miles = this.getMiles(this.restaurant.distance);
  }

  private getMiles(meters): number {
     return meters * 0.000621371192;
  }

}