import { Component, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromRestaurantActions from '../../store/actions';
import * as fromRestaurant from '../../store/reducers';

import { IRestaurant } from '../../models';

@Component({
  selector: 'search-restaurant',
  templateUrl: './search.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class SearchComponent implements OnInit {

  @Input() term: string;
  @Input() location: string;
  @Input() sortby: string;

  constructor(private store: Store<fromRestaurant.RestaurantState>) { }

  ngOnInit() {
  }
  
  setSortBy(sortby: string) {
    this.store.dispatch(new fromRestaurantActions.SearchSetSortByAction(sortby));
  }
  
  setTerm(term: string) {
    this.store.dispatch(new fromRestaurantActions.SearchSetTermAction(term));
  }

  setLocation(location: string) {
    this.store.dispatch(new fromRestaurantActions.SearchSetLocationAction(location));
  }
  
  searchRestaurants(form) {
    if (form.valid) {
      console.log(form.value)
      const { location, term, sortby } = form.value;
      this.store.dispatch(new fromRestaurantActions.SearchRestaurantsAction(location, term, sortby))
    }
  }

}
