import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';


import { Apollo } from 'apollo-angular';
import { WatchQueryOptions } from 'apollo-client';

import gql from 'graphql-tag';
const SearchRestaurants = gql`
query yelp($term: String!, $location: String!, $sortby: String!, $limit: Int) {
	restaurants: searchForBusiness(term: $term, location: $location, sortby: $sortby, limit: $limit) {
		businesses {
  		...businessInfo
		}
	}
}
fragment businessInfo on Business {
  id,
  name,
  image_url,
  url,
  price,
  phone,
  rating,
  review_count,
  distance,
  categories {
    title
  },
  location {
    display_address
  }
}

`;

interface QueryResponse {
  data: any;
}

@Injectable()
export class RestaurantService {

  constructor(private apollo: Apollo) {}

  searchRestaurants(location: string, term: string, sortby: string): Observable<any> {
    const query = { query: SearchRestaurants, variables: { location, term, sortby, limit: 50 }};
    console.log(query)
    return this.apollo.watchQuery<QueryResponse>(query).valueChanges
            	.map(({data}) => data );
  }

}
