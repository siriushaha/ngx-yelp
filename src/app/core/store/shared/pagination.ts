import * as _ from 'lodash';

/**
 * Interface to maintain state for pagination
 */
export interface IPaginatedState {
  list?: Array<any>;
  paged_list?: Array<any>;
  filtered_list?: Array<any>;
  page?: number;
  total?: number;
  per?: number;
  pages?: Array<number>;
}

/**
 * Interface to maintain paginated payload
 */
export interface IPaginatedPayload {
  page: number;
  perPage: number;
}

/**
 * Interface to maintain pagination selection
 */
export interface IPaginatedOption {
  GOTO: string;
  NEXT: string;
  PREV: string;
}

/**
 * Interface to maintain pagination defaults
 */
export interface IPaginatedDefault {
  defaultPage: number;
  defaultPer: number;
}

// Return the total number of pages that can be made from `list`.
export const totalPages = (per = 10, list = []) => {
  const total = Math.ceil(list.length / per);
  return total ? total : 0;
};

// Return a slice of all `list` starting at `start` up to `per`
// (or the length of list; whichever comes first).
export const slicedList = (page = 1, per = 10, list = []) => {
  const start = (page - 1) * per;
  const end = per === 0 ? list.length : start + per;

  return end === list.length ?
    list.slice(start) :
    list.slice(start, end);
};

// Return a list of filtered items based on term and filter_by
export const filterList = (state, term, filter_by) => {
  let filteredList: Array<any>;
  const { per, page } = state;
  if (term != null) {
    filteredList =  state.list.filter((item) => filter_by(item));
  } else {
    filteredList = [...state.list];
  }
  const cachedList: Array<any> = (per < filteredList.length) ? slicedList(page, per, filteredList) : [...filteredList];
  const newState = Object.assign({}, state, {
    filtered_list: filteredList,
    paged_list: cachedList,
    page: 1
  });
  return newState;
};

// Remove list of selected items
export const removeList = (state, filter_by) => {
  const newList: Array<any> = state.list.filter(item => !filter_by(item, state.selected_list));
  const newState = initList(state, newList);
  return newState;
};

// Initialize list of paginated items
export const initList = (state, new_list) => {
  const { per } = state;
  const page = 1;
  const total: number = totalPages(per, new_list);
  const pages: Array<number> = _.range(page, total + 1);
  const cachedList = (per < new_list.length) ? slicedList(page, per, new_list) : [...new_list];
  const newState = Object.assign({}, state, {
    list: new_list,
    paged_list: cachedList,
    filtered_list: [...new_list],
    total: total,
    page: page,
    pages: pages,
    list_length: new_list.length
  });
  return newState;
};

/**
 * It will do pagination.
 * @param state State
 * @param action Action
 */
export function paginateList(state: any, action: any) {
  const { filtered_list, page, per, total } = state;
  let newState: IPaginatedState;
  let cachedList;

  if (page === action.payload.page && per === action.payload.perPage) {
    return state;
  }
  cachedList = (action.payload.perPage < filtered_list.length) ? slicedList(action.payload.page, action.payload.perPage, filtered_list) : [...filtered_list];
  newState = Object.assign({}, state, {
    page: action.payload.page,
    per: action.payload.perPage,
    paged_list: cachedList,
  });
  return newState;
}