import { PaginationActionTypes } from '../actions/pagination.actions';
import { PayloadAction } from '../actions/payload-action.actions';

// Return the total number of pages that can be made from `list`.
export const totalPages = (per = 10, list = []) => {
  const total = Math.ceil(list.length / per);
  return total ? total : 0;
};

// Return a slice of all `list` starting at `start` up to `per`
// (or the length of list; whichever comes first).
export const slicedList = (page = 1, per = 10, list = []) => {
  const start = (page - 1) * per;
  const end = per === 0 ? list.length : start + per;

  return end === list.length ?
    list.slice(start) :
    list.slice(start, end);
};

/**
 * Interface to maintain state for pagination
 */
export interface IPaginatedState {
  list?: Array<any>;
  paged_list?: Array<any>;
  filtered_list?: Array<any>;
  page?: number;
  total?: number;
  per?: number;
  pages?: Array<number>;
  list_length?: number;
}

/**
 * Interface to maintain paginated payload
 */
export interface IPaginatedPayload {
  page: number;
  perPage: number;
}

/**
 * Interface to maintain pagination selection
 */
export interface IPaginatedOption {
  GOTO: string;
  NEXT: string;
  PREV: string;
}

/**
 * Interface to maintain pagination defaults
 */
export interface IPaginatedDefault {
  defaultPage: number;
  defaultPer: number;
}

/**
 * Exported high-order reducer for pagination
 * @param reducer
 * @param {any} GOTO
 * @param {any} NEXT
 * @param {any} PREV
 * @param {any} defaultPage
 * @param {any} defaultPer
 * @param {any} defaultTotal
 * @returns {(state: IPaginatedState, action: PayloadAction) => (IPaginatedState | any | IPaginatedState | any | IPaginatedState | any | any)}
 */
// params:
// 1. the reducer being augmented
// 2. definitions of action types
// 3. options
export const paginate = (
  reducer,
  {
    GOTO = PaginationActionTypes.GOTO,
    NEXT = PaginationActionTypes.NEXT,
    PREV = PaginationActionTypes.PREV
  } = {},
  {
    defaultPage = 1,
    defaultPer = 10,
    defaultTotal = 0
  } = {}
) => {

  const initialPaginatedState: IPaginatedState = {
    list: [],
    paged_list: [],
    filtered_list: [],
    page: defaultPage,
    total: defaultTotal,
    per: defaultPer
  };

  return (state: IPaginatedState = initialPaginatedState, action: PayloadAction) => {
    const { filtered_list, page, per, total } = state;

    let newState: IPaginatedState;
    let cachedList;

    switch (action.type) {

      case GOTO:
        if (page === action.payload.page && per === action.payload.perPage) {
          return state;
        }
        cachedList = (action.payload.perPage < filtered_list.length) ? slicedList(action.payload.page, action.payload.perPage, filtered_list) : [...filtered_list];
        newState = Object.assign({}, state, {
          page: action.payload.page,
          per: action.payload.perPage,
          paged_list: cachedList,
        });
        return reducer(newState, action);

      case NEXT:
        if (page >= total) {
          return state;
        }
        const nextPage = page + 1;
        cachedList = slicedList(nextPage, per, filtered_list);
        // if (nextPage > state.list.length - 1) nextPage = 0;let
        newState = Object.assign({}, state, {
          page: nextPage,
          paged_list: cachedList,
        });
        return reducer(newState, action);

      case PREV:
        if (page <= 1) {
          return state;
        }
        const prevPage = page - 1;
        cachedList = slicedList(prevPage, per, filtered_list);
        // if  (prevPage < 0) prevPage = state.list.length - 1;
        newState = Object.assign({}, state, {
          page: prevPage,
          paged_list: cachedList
        });
        return reducer(newState, action);

      // Setup the default list and cache and calculate the total.
      default:
        // console.log(`in paginate default`)
        return reducer(state, action);
    }
  };
};