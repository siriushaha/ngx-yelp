/**
 * Defined constants used for pagination
 * @type {{GOTO: string; NEXT: string; PREV: string}}
 */
export enum PaginationActionTypes {
  GOTO = 'GOTO',
  NEXT = 'NEXT',
  PREV = 'PREV'
};
