import { Action } from '@ngrx/store';

/**
 * Interface for actions containing payload
 */
export interface PayloadAction extends Action {
  payload?: any;
}
