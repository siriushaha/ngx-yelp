import { environment } from '../../../environments/environment';

import { Apollo } from 'apollo-angular';
import { ApolloLink, from} from 'apollo-link';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-angular-link-http';
import { HttpHeaders } from '@angular/common/http';
import { createUploadLink } from 'apollo-upload-client';

const GRAPHQL_URL = 'http://localhost:8082/graphql';

/**
 * Function to create ApolloLink for token authentication
 * @type {ApolloLink}
 */
const authMiddleware = new ApolloLink((operation, forward) => {
  const token: string = localStorage.getItem('auth-token');
  if (token) {
    const authHeader = `Bearer ${token}`;
    operation.setContext({
      headers: new HttpHeaders().set('Authorization', authHeader)
    });
  }
  return forward(operation);
});

/**
 * Function to create Apollo client to support GraphQL queries and mutations without file uploads
 * @param {Apollo} apollo
 * @param {HttpLink} httpLink
 */
const create = (apollo: Apollo, httpLink: HttpLink): void => {
  const http = httpLink.create({ uri: GRAPHQL_URL });

  apollo.create({
    link: from([http]),
    cache: new InMemoryCache()
  });
};

/**
 * Function to create Apollo client to support mutations with file uploads
 * @param {Apollo} apollo
 * @param {HttpLink} httpLink
 */
const createWithFileUpload = (apollo: Apollo): void => {
  const httpUpload = createUploadLink({ uri: GRAPHQL_URL, headers:  new HttpHeaders().set('Content-Type', 'multipart/form-data') });

  apollo.create({
    link: from([authMiddleware, httpUpload]),
    cache: new InMemoryCache()
  }, 'fileupload');
};

/**
 * Exported GraphQL client
 * @type {{create: ((apollo: Apollo, httpLink: HttpLink) => void); createWithFileUpload: ((apollo: Apollo) => void)}}
 */
export const GraphQLClient = {
  createWithFileUpload,
  create
};
